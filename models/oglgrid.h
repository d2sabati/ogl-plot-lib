#ifndef OGLGRID_H
#define OGLGRID_H

#include <QtOpenGL>
#include <glm/glm.hpp>
#include <cstdio>
#include <iostream>
#include <math.h>

#ifndef MAX
#define MAX 1
#endif

#ifndef MIN
#define MIN 0
#endif

using namespace std;

class CPoint3D
{
public:
   float x, y, z;
   CPoint3D () { x=y=z=0; }
   CPoint3D (float c1, float c2, float c3)
   {
      x = c1;      y = c2;      z = c3;
   }
   CPoint3D& operator=(const CPoint3D& pt)
   {
      x = pt.x;   z = pt.z;   y = pt.y;
      return *this;
   }
   CPoint3D (const CPoint3D& pt)
   {
      *this = pt;
   }
   void Translate(double cx, double cy, double cz)
   {
       x+=cx;
       y+=cy;
       z+=cz;
   }
};

class OGLGrid : public QObject
{
    Q_OBJECT
public:
    OGLGrid();
    //convertit les valeurs du point sur le repere de la grid
    CPoint3D convertCoord(CPoint3D &);
    //set les valeurs d´autoRange
    void setAutoRange(double x, double y, double z, bool isPlotAvailable);
    //ajoute les axes et labels correspondants
    void drawAxisBox();
    void drawAxisGrid();
    void drawAxisLabels();
    //permet de set la couleur courante d´OPENGL
    void setColor(QColor c);
    void set3D(bool newVal);
    //GETTER ET SETTER DES LABELS
    void setXLabel(QString);
    void setYLabel(QString);
    void setZLabel(QString);
    QString getXLabel(){return m_XLabel;}
    QString geYXLabel(){return m_YLabel;}
    QString getZLabel(){return m_ZLabel;}
    //GETTER ET SETTER DU NOMBRE DE GRID
    void setXGridNumber(int);
    void setYGridNumber(int);
    void setZGridNumber(int);
    int getXGridNumber(){return m_gridX;}
    int getYGridNumber(){return m_gridY;}
    int getZGridNumber(){return m_gridZ;}
    double** getRanges();
private:
    //appeler pour afficher les labels (envoie le signal drawText)
    void printText(CPoint3D, QString, QColor);
    //arrondie la valeur affichée
    float roundValue(float nb, int po);

    QString m_XLabel;
    QString m_YLabel;
    QString m_ZLabel;

    int m_gridX; 	// XGridNumber
    int m_gridY;	// YGridNumber
    int m_gridZ;    // ZGridNumber

    double dRangeX[2];
    double dRangeY[2];
    double dRangeZ[2];

    double dAutoRangeX[2];
    double dAutoRangeY[2];
    double dAutoRangeZ[2];

    double sizeAxis;
    double distAxis;

    bool is3D;
public slots:
    //check si le point est dans la range des Axes
    bool ptInRange(CPoint3D &);
    //check si le point est dans la box
    bool ptInAxisBox(CPoint3D &);
    //permet de set les valeurs min/max d´affichages
    void setRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);
    //select AutoRange pour l´affichage
    void autoRange();
    //set la taille des Axes
    void setAxisSize(double newSize);
signals:
    void drawText(GLfloat x, GLfloat y, GLfloat z, QString text, QColor fontColor);
    void updateGraph();
};

#endif // OGLGRID_H
