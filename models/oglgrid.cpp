#include "oglgrid.h"

static QColor red(255,25,25), green(25,255,25), blue(25,25,255);

OGLGrid::OGLGrid()
{
    m_XLabel = "X";
    m_YLabel = "Y";
    m_ZLabel = "Z";

    m_gridX = 6;
    m_gridY = 6;
    m_gridZ = 6;

    setRange(0,1,0,1,0,1);

    dAutoRangeX[MIN] = 0;
    dAutoRangeX[MAX] = 0;
    dAutoRangeY[MIN] = 0;
    dAutoRangeY[MAX] = 0;
    dAutoRangeZ[MIN] = 0;
    dAutoRangeZ[MAX] = 0;

    sizeAxis = 0.5f;
    distAxis = sizeAxis*2;

    is3D = true;
}

void OGLGrid::set3D(bool newVal){
    is3D = newVal;
}

void OGLGrid::setRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax){
    if (zmin==zmax)
        zmax += 1;
    dRangeX[MIN]=xmin;
    dRangeX[MAX]=xmax;
    dRangeY[MIN]=ymin;
    dRangeY[MAX]=ymax;
    dRangeZ[MIN]=zmin;
    dRangeZ[MAX]=zmax;
}

void OGLGrid::autoRange(){
    setRange(dAutoRangeX[MIN],dAutoRangeX[MAX],
             dAutoRangeY[MIN],dAutoRangeY[MAX],
             dAutoRangeZ[MIN],dAutoRangeZ[MAX]);
    updateGraph();
}

void OGLGrid::drawAxisBox(){
    glLineWidth(1.0f);

    glBegin(GL_LINE_STRIP);
        // x axis
        setColor(red);
        glVertex3f(-sizeAxis, -sizeAxis, -sizeAxis);
        glVertex3f(sizeAxis, -sizeAxis, -sizeAxis);
        glVertex3f(sizeAxis, -sizeAxis, -sizeAxis);
        glVertex3f(sizeAxis, sizeAxis, -sizeAxis);
        glVertex3f(sizeAxis, sizeAxis, -sizeAxis);
        glVertex3f(-sizeAxis, sizeAxis, -sizeAxis);
        if(is3D){
            // y axis
            setColor(green);
            glVertex3f(-sizeAxis, -sizeAxis, -sizeAxis);
            glVertex3f(-sizeAxis, sizeAxis, -sizeAxis);
            glVertex3f(-sizeAxis, sizeAxis, -sizeAxis);
            glVertex3f(-sizeAxis, sizeAxis, sizeAxis);
            glVertex3f(-sizeAxis, sizeAxis, sizeAxis);
            glVertex3f(-sizeAxis, -sizeAxis, sizeAxis);
            // z axis
            setColor(blue);
            glVertex3f(-sizeAxis, -sizeAxis, -sizeAxis);
            glVertex3f(-sizeAxis, -sizeAxis, sizeAxis);
            glVertex3f(-sizeAxis, -sizeAxis, sizeAxis);
            glVertex3f(sizeAxis, -sizeAxis, sizeAxis);
            glVertex3f(sizeAxis, -sizeAxis, sizeAxis);
            glVertex3f(sizeAxis, -sizeAxis, sizeAxis);
            glVertex3f(sizeAxis, -sizeAxis, -sizeAxis);
        }
    glEnd();
}

void OGLGrid::drawAxisGrid(){
    glBegin(GL_LINES);

        float X,Y,Z;
        int i;

        // Draw X Grid
        for ( i = 0 ; i <= m_gridX ; i++)
        {
            X = ((float)i*distAxis)/m_gridX;

            // x axis
            setColor(red);

            glVertex3f(X-sizeAxis, -sizeAxis,	-sizeAxis);
            glVertex3f(X-sizeAxis,  sizeAxis,	-sizeAxis);

            if(is3D){
                setColor(blue);

                glVertex3f(X-sizeAxis, -sizeAxis,	-sizeAxis);
                glVertex3f(X-sizeAxis, -sizeAxis,	 sizeAxis);
            }
        }
        if(!is3D){
            // Draw Y Grid
            for ( i = 0 ; i <= m_gridY ; i++){

                Y = ((float)i*distAxis)/m_gridY;

                // y axis
                setColor(red);

                glVertex3f(-sizeAxis,	Y-sizeAxis, -sizeAxis);
                glVertex3f( sizeAxis,	Y-sizeAxis, -sizeAxis);
            }
        }else{
            // Draw Y Grid
            for ( i = 0 ; i <= m_gridY ; i++)
            {

                Y = ((float)i*distAxis)/m_gridY;

                // y axis
                setColor(red);

                glVertex3f(-sizeAxis,	Y-sizeAxis, -sizeAxis);
                glVertex3f( sizeAxis,	Y-sizeAxis, -sizeAxis);

                setColor(green);

                glVertex3f(-sizeAxis,	Y-sizeAxis, -sizeAxis);
                glVertex3f(-sizeAxis,	Y-sizeAxis,  sizeAxis);

            }

            // Draw Z Grid
            for ( i = 0 ; i <= m_gridZ ; i++)
            {
                Z = ((float)i*distAxis)/m_gridZ;

                // z axis
                setColor(blue);

                glVertex3f(-sizeAxis  ,-sizeAxis  ,Z-sizeAxis);
                glVertex3f( sizeAxis  ,-sizeAxis  ,Z-sizeAxis);

                setColor(green);

                glVertex3f(-sizeAxis  ,-sizeAxis  ,Z-sizeAxis);
                glVertex3f(-sizeAxis  , sizeAxis  ,Z-sizeAxis);
            }
        }

    glEnd();
}

void OGLGrid::drawAxisLabels()
{
    float X,Y,Z,res;
    int i;
    char str[50];
    CPoint3D pt;

    // Draw X Grid Label
    res = (dRangeX[MAX] - dRangeX[MIN]) / m_gridX ;

    for ( i = 0 ; i <= m_gridX ; i++)
    {
        X = dRangeX[MIN] + (res * (float)i);
        sprintf(str,"%g",X);

        X = ((float)i*distAxis)/m_gridX;

        pt=CPoint3D(-sizeAxis,-sizeAxis,sizeAxis);
        double xy = 0.0;
        if(!is3D){
            xy = -sizeAxis/5.0f;
        }
        pt.Translate(X,xy,sizeAxis/5.0f);

        printText(pt, str, red);
    }

    // Draw Y Grid Label
    res = (dRangeY[MAX] - dRangeY[MIN]) / m_gridY ;

    for ( i = 0 ; i <= m_gridY ; i++)
    {
        Y = dRangeY[MIN] + (res * (float)i) ;
        sprintf(str,"%g",Y);

        Y = ((float)i*distAxis)/m_gridY;

        double yx = 0.0;
        if(!is3D){
            yx = -sizeAxis/2.5f;
        }

        pt = CPoint3D(-sizeAxis,-sizeAxis,sizeAxis);
        pt.Translate(yx,Y,sizeAxis/5.0f);
        printText(pt, str,green);
    }

    if(is3D){
        // Draw Z Grid Label
        res = (dRangeZ[MAX] - dRangeZ[MIN]) / m_gridZ ;

        for (i=0; i<=m_gridZ; i++)
        {
            Z = dRangeZ[MIN] + (res * (float)i) ;
            sprintf(str,"%g",Z);

            Z = ((float)i*distAxis)/m_gridZ;

            pt = CPoint3D(sizeAxis,-sizeAxis,-sizeAxis);
            pt.Translate (sizeAxis/5.0f,0.0,Z);
            printText(pt, str, blue);
        }
    }

    // Draw Axis Titles
    printText(CPoint3D(0.0f,sizeAxis+sizeAxis/5.0f,-sizeAxis),m_XLabel,red);
    printText(CPoint3D(sizeAxis+sizeAxis/5.0f,0.0f,-sizeAxis),m_YLabel,green);
    if(is3D)printText(CPoint3D(-sizeAxis,sizeAxis+sizeAxis/5.0f,0.0f),m_ZLabel,blue);

    glEnd();

}

float OGLGrid::roundValue(float nb, int po){
    double off=pow(10,po);
    float t = nb-floor(nb);
    if (t>=0.5){
        nb*=off;
        nb = ceil(nb);
        nb/=off;
    }
    else{
        nb*=off;
        nb = floor(nb);
        nb/=off;
    }
    return nb;
}

void OGLGrid::printText(CPoint3D p, QString str, QColor fontColor){
    drawText(p.x,p.y,p.z,str,fontColor);
}

void OGLGrid::setXLabel(QString str){
    m_XLabel = str;
}

void OGLGrid::setYLabel(QString str){
    m_YLabel = str;
}

void OGLGrid::setZLabel(QString str){
    m_ZLabel = str;
}

void OGLGrid::setXGridNumber(int nb){
    m_gridX = nb;
}

void OGLGrid::setYGridNumber(int nb){
    m_gridY = nb;
}

void OGLGrid::setZGridNumber(int nb){
    m_gridZ = nb;
}

CPoint3D OGLGrid::convertCoord(CPoint3D &point){
    float xF=dRangeX[MAX]-dRangeX[MIN];
    float yF=dRangeY[MAX]-dRangeY[MIN];
    float zF=dRangeZ[MAX]-dRangeZ[MIN];

    CPoint3D result;

    result.x = (point.x-dRangeX[MIN])*distAxis/xF - sizeAxis;
    result.y = (point.y-dRangeY[MIN])*distAxis/yF - sizeAxis;
    result.z = (point.z-dRangeZ[MIN])*distAxis/zF - sizeAxis;

    return result;
}

void OGLGrid::setAutoRange(double x, double y, double z, bool isPlotAvailable){
    if(isPlotAvailable){
        if(x<dAutoRangeX[MIN])	dAutoRangeX[MIN]=x;
        if(y<dAutoRangeY[MIN])	dAutoRangeY[MIN]=y;
        if(z<dAutoRangeZ[MIN])	dAutoRangeZ[MIN]=z;

        if(x>dAutoRangeX[MAX])	dAutoRangeX[MAX]=x;
        if(y>dAutoRangeY[MAX])  dAutoRangeY[MAX]=y;
        if(z>dAutoRangeZ[MAX])  dAutoRangeZ[MAX]=z;
    }else{
        cout<<x<<" "<<y<<" "<<z<<endl;
        dAutoRangeX[MIN] = x;
        dAutoRangeY[MIN] = y;
        dAutoRangeZ[MIN] = z;

        dAutoRangeX[MAX] = x;
        dAutoRangeY[MAX] = y;
        dAutoRangeZ[MAX] = z;
    }
}

void OGLGrid::setColor(QColor c){
    float r=float(c.red())/255;
    float g=float(c.green())/255;
    float b=float(c.blue())/255;
    glColor3f(r,g,b);
}

bool OGLGrid::ptInRange(CPoint3D &pt)
{
    if (pt.x>=dRangeX[MIN] && pt.x<=dRangeX[MAX] &&
        pt.y>=dRangeY[MIN] && pt.y<=dRangeY[MAX] &&
        pt.z>=dRangeZ[MIN] && pt.z<=dRangeZ[MAX])
        return true;
    return false;
}

bool OGLGrid::ptInAxisBox(CPoint3D &pt)
{
    float xF=dRangeX[MAX]-dRangeX[MIN];
    float yF=dRangeY[MAX]-dRangeY[MIN];
    float zF=dRangeZ[MAX]-dRangeZ[MIN];

    CPoint3D point;

    point.x = (pt.x+dRangeX[MIN])/xF + sizeAxis;
    point.y = (pt.y+dRangeY[MIN])/yF + sizeAxis;
    point.z = (pt.z+dRangeZ[MIN])/zF + sizeAxis;

    return (ptInRange(point));
}

void OGLGrid::setAxisSize(double newSize){
    sizeAxis = newSize;
    distAxis = sizeAxis*2;
    updateGraph();
}

double** OGLGrid::getRanges(){
    double** ranges = 0;
    ranges = new double*[3];
    ranges[0] = new double[2];
    ranges[0][MIN] = dRangeX[MIN];
    ranges[0][MAX] = dRangeX[MAX];
    ranges[1] = new double[2];
    ranges[1][MIN] = dRangeY[MIN];
    ranges[1][MAX] = dRangeY[MAX];
    ranges[2] = new double[2];
    ranges[2][MIN] = dRangeZ[MIN];
    ranges[2][MAX] = dRangeZ[MAX];

    return ranges;
}
