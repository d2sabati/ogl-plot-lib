#ifndef OPENGLWINDOW_H
#define OPENGLWINDOW_H

#include <QOpenGLWidget>
#include <QTextDocument>
#include <models/oglgrid.h>
#include <algorithm>

// Enum to describe control's actions constants
enum TrackModeState {NoTrack, Zoom, Rotate, RangeZoom};
// Enum to describe control's projection style constants
enum ProjectionStyle {Perspective, Orthographic};
// Enum to describe control's border style constants
enum Borderstyle {none = 0, bump = 1, etched = 2, raised = 3};
// Enum to describe control's line style constants
enum LineType {Lines, Points, LinePoint, Surface};

using namespace std;

// Define a template class for a vector of 3D Points.
typedef vector<CPoint3D> POINTVECTOR;

class CElement{
public:
    CElement(){
         min=max=CPoint3D(0,0,0);
         m_isPlotAvailable = false ;
         m_show = true;
         m_fill = true;
         m_flat = false;
         m_lights = false;
         m_label = false;

         ////// Initial lighting params //////
         m_lightParam[0] = 40;	// X position
         m_lightParam[1] = 100;	// Y position
         m_lightParam[2] = 80;	// Z position
         m_lightParam[3] = 80;	// Ambient light
         m_lightParam[4] = 50;	// Diffuse light
         m_lightParam[5] = 50;	// Specular light
         m_lightParam[6] = 50;	// Ambient material
         m_lightParam[7] = 50;	// Diffuse material
         m_lightParam[8] = 40;	// Specular material
         m_lightParam[9] = 70;	// Shininess material
         m_lightParam[10] = 50;	// Emission material

         m_lineColor = m_pointColor = QColor(100,100,100);
         m_type  = LinePoint;
         m_lineWidth = 0.0f;
         m_pointSize = 0.0f;
    }

    bool m_label;
    bool m_isPlotAvailable;
    bool m_show;
    bool m_fill;
    bool m_flat;
    bool m_lights;
    int	m_lightParam[11];	// Graphics dimension (along X-axis)
    QColor m_lineColor;
    QColor m_pointColor;
    LineType m_type;
    GLfloat	 m_lineWidth;
    GLfloat  m_pointSize;

    CPoint3D min,max;

    POINTVECTOR m_pointList;
};

// Define a template class for a vector of Elements.
typedef vector<CElement> ELEMENTVECTOR;

class OpenGLWindow : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    OpenGLWindow(QWidget *parent = 0);
    ~OpenGLWindow();
    //add a new graph to the scene, return it´s id
    int newElement();
    //add a new graph and fill it with the points given
    int newElement(vector<double[3]> points);
    //delete a graph from it´s id
    void deleteElement(int id);
    //add a point to the element id
    void plotXYZ(int id, double x, double y, double z);
    //clear all elements
    void clearGraph();
    //update the view
    void updateScene();
    //set if view is 3D or 2D
    void set3D(bool newVal);
    //show or not the grid
    void setDisplayGrid(bool newVal);
    //getter / setter projection
    int getProjectionType();
    void setProjectionType(int t);
    //getter / setter for element graph view
    bool getElementLabel(int id);
    void setElementLabel(int id, bool newVal);
    bool getElementLight(int id);
    void setElementLight(int id, bool newVal);
    bool getElementSurfaceFlat(int id);
    void setElementSurfaceFlat(int id, bool newVal);
    bool getElementSurfaceFill(int id);
    void setElementSurfaceFill(int id, bool newVal);
    bool getElementShow(int id);
    void setElementShow(int id, bool newVal);
    int getElementType(int id);
    void setElementType(int id, int t);
    GLfloat getElementPointSize(int id);
    void setElementPointSize(int id, GLfloat newVal);
    GLfloat getElementLineWidth(int id);
    void setElementLineWidth(int id, GLfloat newVal);
    QColor getElementPointColor(int id);
    void setElementPointColor(int id, QColor newVal);
    QColor getElementLineColor(int id);
    void setElementLineColor(int id, QColor newVal);
protected:
    //functions overloaded from QGLWidget
    void initializeGL();
    void resizeGL(int w, int h);
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    //void mouseDoubleClickEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
private:
    //allow the perspective view
    void perspectiveGL( GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar );
    //create gradient for the background
    void createGradient();
    //create an opengl object
    void createGLObject();
    //draw the background
    void drawBackground();
    //draw the scene
    void drawScene();
    //draw everything grid related
    void drawGrid();
    //plot the graphs elements
    void plotAll();
    //void drawLegend();
    void findDistValue();
    //draw a 3D text (not used)
    //void drawText(QString text);
    //render the text at given position (redefinition from the qglWidget one)
    void renderText(double x, double y, double z, const QString txt, const QColor fontColor);
    //convert widget coordinate to opengl coordinates
    inline GLint project(double objx, double objy, double objz, GLdouble * winx, GLdouble * winy, GLdouble * winz);
    inline void transformPoint(GLdouble out[4], const GLdouble m[16], const GLdouble in[4]);
    //convert qglwidget functions to qopenglwidget
    void qglColor(QColor color);
    void qglClearColor(QColor color);

    //Axis *xAxis, *yAxis, *zAxis;
    OGLGrid *grid;

    bool isPlotAvailable;
    ELEMENTVECTOR m_ElementList;

    QFont font;
    QColor color;
    GLuint glObject;
    QRadialGradient gradient;

    short m_projection;

    bool m_mouseCaptured;
    bool showGrid;
    bool is3D;
    short m_trackState;
    GLfloat m_rotationX, m_rotationY;
    GLfloat m_xPos, m_yPos;
    GLfloat m_scaling;
public slots:
    //print text with QPainter using renderText from QGLWidget
    void printText(GLfloat x, GLfloat y, GLfloat z, QString text, QColor fontColor);
signals:
    //check if pt is in the axes range
    bool ptInRange(CPoint3D &);
    //check if pt is in the Axis Box
    bool ptInAxisBox(CPoint3D &);
    //set signal to grid to setRange with values min/max for each axis
    void setRange(double xMin, double xMax, double yMin, double yMax, double zMin, double zMax);
    //select AutoRange for the drawing
    void autoRange();
    //set size of axis
    void setAxisSize(double size);
};
#endif // OPENGLWINDOW_H
