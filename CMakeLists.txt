if(NOT CCT_FOUND)
   message(FATAL_ERROR "CCT IS REQUIRED")
endif()

set(name ogl-plot-lib)

set(${name}-src
    openglwindow.cpp
    models/oglgrid.cpp)

CCT_ADD_PREFIX(${name}-src ${CMAKE_CURRENT_LIST_DIR})


set(${name}-inc
  ${CMAKE_CURRENT_LIST_DIR} CACHE INTERNAL ""
)

set(${name}-lib "" CACHE INTERNAL "")
