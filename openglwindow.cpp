#include "openglwindow.h"

#include <QMouseEvent>
#include <iostream>

using namespace std;

OpenGLWindow::OpenGLWindow(QWidget *parent)
    : QOpenGLWidget(parent)
{
    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    setFormat(format);

    grid = new OGLGrid();

    isPlotAvailable = false;
    showGrid = true;
    is3D = true;
    m_projection = Perspective;
    m_trackState = NoTrack;
    m_rotationX = 38.0;
    m_rotationY = -58.0;
    m_scaling = 1.0;
    createGradient();

    connect(this, SIGNAL(ptInRange(CPoint3D&)),grid,SLOT(ptInRange(CPoint3D&)));
    connect(this,SIGNAL(ptInAxisBox(CPoint3D&)),grid,SLOT(ptInAxisBox(CPoint3D&)));
    connect(this, SIGNAL(autoRange()), grid, SLOT(autoRange()));
    connect(this,SIGNAL(setRange(double,double,double,double,double,double)),grid,SLOT(setRange(double,double,double,double,double,double)));
    connect(this,SIGNAL(setAxisSize(double)),grid,SLOT(setAxisSize(double)));
    connect(grid, SIGNAL(drawText(GLfloat,GLfloat,GLfloat,QString,QColor)),this,SLOT(printText(GLfloat,GLfloat,GLfloat,QString,QColor)));
    connect(grid, SIGNAL(updateGraph()),this,SLOT(update()));
}

void OpenGLWindow::initializeGL()
{
    initializeOpenGLFunctions();
}

void OpenGLWindow::resizeGL(int width, int height)
{
    GLfloat fMaxObjSize, fAspect;
    GLfloat fNearPlane, fFarPlane;

    glViewport(-width/2, -height/2, width/2, height/2);

    fNearPlane = 1.0f;
    fFarPlane = 20.0f;
    fMaxObjSize = 3.0f;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // update the camera
    if (m_projection == Orthographic)
        glOrtho(-1,1,-1,1,fNearPlane,fFarPlane);
    else
        perspectiveGL(30.0f, fAspect, fNearPlane, fFarPlane);
    glMatrixMode(GL_MODELVIEW);
}

void OpenGLWindow::set3D(bool newVal){
    is3D = newVal;
    grid->set3D(newVal);
    if(!newVal){
        m_rotationX = 0.0f;
        m_rotationY = 0.0f;
    }
}

void OpenGLWindow::perspectiveGL( GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar )
{
    const GLdouble pi = 3.1415926535897932384626433832795;
    GLdouble fW, fH;

    fH = tan( fovY / 360 * pi ) * zNear;
    fW = fH * aspect;

    glFrustum( -fW, fW, -fH, fH, zNear, zFar );
}

void OpenGLWindow::createGradient()
{
    gradient.setCoordinateMode(QGradient::ObjectBoundingMode);
    gradient.setCenter(0.45, 0.50);
    gradient.setFocalPoint(0.40, 0.45);
    gradient.setColorAt(0.0, QColor(105, 146, 182));
    gradient.setColorAt(0.4, QColor(81, 113, 150));
    gradient.setColorAt(0.8, QColor(16, 56, 121));
}

/*void OpenGLWindow::drawText(QString text){
    QPainterPath path;
    glDisable(GL_LIGHTING);
    QFont font("Arial", 40);
    path.addText(QPointF(0, 0), font, text);
    QList<QPolygonF> poly = path.toSubpathPolygons();
    for (QList<QPolygonF>::iterator i = poly.begin(); i != poly.end(); i++){
        glBegin(GL_LINE_LOOP);
        for (QPolygonF::iterator p = (*i).begin(); p != i->end(); p++)
            glVertex3f(p->rx()*0.1f, -p->ry()*0.1f, 0);
        glEnd();
    }
    glEnable(GL_LIGHTING);
}*/

void OpenGLWindow::renderText(double x, double y, double z, const QString text, const QColor fontColor)
{
    // Identify x,y and z coordinates to render text within widget
    int height = this->height();
    GLdouble textPosX = 0, textPosY = 0, textPosZ = 0;

    project(x,y,z,&textPosX, &textPosY, &textPosZ);

    textPosY = height - textPosY; // y is inverted

    // Render text
    QPainter painter(this);
    painter.setPen(fontColor);
    painter.setFont(font);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    painter.drawText(textPosX, textPosY, text); // z = pointT4.z + distOverOp / 4
    painter.end();
}

inline GLint OpenGLWindow::project(double objx, double objy, double objz, GLdouble *winx, GLdouble *winy, GLdouble *winz){
    GLdouble in[4], out[4];

    GLdouble model[4][4], proj[4][4];
    GLint viewport[4];
    // Get the Model matrix
    glGetDoublev(GL_MODELVIEW_MATRIX, &model[0][0]);
    // Get the Projection matrix
    glGetDoublev(GL_PROJECTION_MATRIX, &proj[0][0]);
    // Get the View matrix
    glGetIntegerv(GL_VIEWPORT, &viewport[0]);

    in[0] = objx;
    in[1] = objy;
    in[2] = objz;
    in[3] = 1.0;

    transformPoint(out, &model[0][0], in);
    transformPoint(in, &proj[0][0], out);

    if (in[3] == 0.0)
        return GL_FALSE;

    in[0] /= in[3];
    in[1] /= in[3];
    in[2] /= in[3];

    *winx = viewport[0] + (1 + in[0]) * viewport[2] / 2;
    *winy = viewport[1] + (1 + in[1]) * viewport[3] / 2;

    *winz = (1 + in[2]) / 2;
    return GL_TRUE;
}

inline void OpenGLWindow::transformPoint(GLdouble out[4], const GLdouble m[16], const GLdouble in[4])
{
#define M(row,col)  m[col*4+row]
    out[0] =
        M(0, 0) * in[0] + M(0, 1) * in[1] + M(0, 2) * in[2] + M(0, 3) * in[3];
    out[1] =
        M(1, 0) * in[0] + M(1, 1) * in[1] + M(1, 2) * in[2] + M(1, 3) * in[3];
    out[2] =
        M(2, 0) * in[0] + M(2, 1) * in[1] + M(2, 2) * in[2] + M(2, 3) * in[3];
    out[3] =
        M(3, 0) * in[0] + M(3, 1) * in[1] + M(3, 2) * in[2] + M(3, 3) * in[3];
#undef M
}

OpenGLWindow::~OpenGLWindow()
{
    makeCurrent();
    glDeleteLists(glObject, 1);
}

void OpenGLWindow::paintEvent(QPaintEvent * /* event */)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    drawBackground();
    drawScene();
    //drawLegend();
}

void OpenGLWindow::drawBackground()
{
    QPainter painter(this);
    painter.setPen(Qt::NoPen);
    painter.setBrush(gradient);
    painter.drawRect(rect());
}

void OpenGLWindow::drawScene()
{
    font = QFont("Times", m_scaling*0.1);
    glPushAttrib(GL_ALL_ATTRIB_BITS);

    GLfloat fMaxObjSize, fAspect;
    GLfloat fNearPlane, fFarPlane;

    fNearPlane = 1.0f;
    fFarPlane = 20.0f;
    fMaxObjSize = 3.0f;

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    // update the camera
    if (m_projection == Orthographic)
        glOrtho(-1,1,-1,1,fNearPlane,fFarPlane);
    else
        perspectiveGL(30.0f, fAspect, fNearPlane, fFarPlane);
    glMatrixMode(GL_MODELVIEW);

    glPushMatrix();
    glLoadIdentity();

    glScalef(m_scaling, m_scaling, m_scaling);

    glRotatef(m_rotationX, 1.0, 0.0, 0.0);
    glRotatef(m_rotationY, 0.0, 1.0, 0.0);

    qglColor(Qt::white);

    if(showGrid)
        drawGrid();

    plotAll();

    qglColor(QColor(25,25,255));

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    glPopAttrib();
}

void OpenGLWindow::updateScene(){
    update();
}

void OpenGLWindow::printText(GLfloat x, GLfloat y, GLfloat z, QString text, QColor fontColor){
    renderText(x,y,z,text,fontColor);
}

void OpenGLWindow::drawGrid(){
    grid->drawAxisBox();
    grid->drawAxisGrid();
    grid->drawAxisLabels();
}

void OpenGLWindow::plotXYZ(int id, double x, double y, double z){
    grid->setAutoRange(x,y,z,isPlotAvailable);

    CPoint3D point(x,y,z);

    // Gets the position of the element by index.
    m_ElementList[id].m_pointList.push_back(point);

    if(!m_ElementList[id].m_isPlotAvailable){
       m_ElementList[id].min.x = point.x;
       m_ElementList[id].max.x = point.x;
       m_ElementList[id].min.y = point.y;
       m_ElementList[id].max.y = point.y;
       m_ElementList[id].min.z = point.z;
       m_ElementList[id].min.z = point.z;
       m_ElementList[id].m_isPlotAvailable = true;
    }else{
        if(m_ElementList[id].min.x > point.x)
                 m_ElementList[id].min.x=point.x ;

        if(m_ElementList[id].min.y > point.y)
                 m_ElementList[id].min.y=point.y ;

        if(m_ElementList[id].min.z > point.z)
                 m_ElementList[id].min.z=point.z ;

        if(m_ElementList[id].max.x < point.x)
                 m_ElementList[id].max.x=point.x ;

        if(m_ElementList[id].max.y < point.y)
                 m_ElementList[id].max.y=point.y ;

        if(m_ElementList[id].max.z < point.z)
                 m_ElementList[id].max.z=point.z ;
    }

    if(!isPlotAvailable)
        isPlotAvailable = true;
}

void OpenGLWindow::clearGraph(){
    if(m_ElementList.empty())
        return;
    else
        m_ElementList.clear();
    isPlotAvailable = false;
}

int OpenGLWindow::newElement(){
    CElement elem;
    m_ElementList.push_back(elem);
    return m_ElementList.size()-1;
}

int OpenGLWindow::newElement(vector<double[3]> points){
    CElement elem;
    m_ElementList.push_back(elem);
    int id = m_ElementList.size()-1;
    vector<double[3]>::iterator point;
    for(point = points.begin(); point!= points.end();point++){
        plotXYZ((*point)[0],(*point)[1],(*point)[2],id);
    }
    return id;
}

void OpenGLWindow::deleteElement(int id){
    if (m_ElementList.empty() || m_ElementList.size() < id - 1)
        return;
    else
        m_ElementList.erase (m_ElementList.begin() + id - 1);
}

void OpenGLWindow::plotAll(){

    if(m_ElementList.empty())
        return;

    ELEMENTVECTOR::iterator theElement;

    for (theElement = m_ElementList.begin();
         theElement != m_ElementList.end();
         theElement++){

        // Prevent plotting of non-existing data
        if (!theElement->m_isPlotAvailable)
           continue;
        // Check show state of the element
        if (!theElement->m_show)
            continue;

        if (theElement->m_lights){
          //setLight(theElement);
          glEnable(GL_LIGHTING);  // Lighting will be used
          glEnable(GL_LIGHT0);    // Only one (first) source of light
          glEnable(GL_DEPTH_TEST);// The depth of the Z-buffer will be taken into account
          glEnable(GL_COLOR_MATERIAL);// Material colors will be taken into account
        }

        POINTVECTOR::iterator aPoint;

        // Draw Lines
        if (theElement->m_type == Lines || theElement->m_type == LinePoint){
            glLineWidth(theElement->m_lineWidth);
            glBegin(GL_LINE_STRIP);

            qglColor(theElement->m_lineColor);

            aPoint = theElement->m_pointList.begin();
            for (aPoint; aPoint != theElement->m_pointList.end(); aPoint++){
                if (ptInRange((*aPoint))){
                    CPoint3D pt = grid->convertCoord((*aPoint));
                    //cout<<"point: "<<pt.x<<" "<<pt.y<<" "<<pt.z<<endl;
                    glVertex3f(pt.x,pt.y,pt.z);
                    if(theElement->m_label)
                        renderText(pt.x,pt.y,pt.z,QString::number(pt.z),Qt::black);
                }
            }
            glEnd();
        }

        // Now draw points.
        if ( theElement->m_type == Points || theElement->m_type == LinePoint ){
            glPointSize(theElement->m_pointSize);
            glBegin(GL_POINTS);
            qglColor(theElement->m_pointColor);

            aPoint = theElement->m_pointList.begin();
            for (aPoint; aPoint != theElement->m_pointList.end(); aPoint++)
            {
                if (ptInRange((*aPoint)))
                {
                    CPoint3D pt = grid->convertCoord((*aPoint));
                    glVertex3f(pt.x,pt.y,pt.z);
                    if(theElement->m_label){
                        cout<<"label is set"<<pt.x<<pt.y<<pt.z<<endl;
                        renderText((*aPoint).x,(*aPoint).y,(*aPoint).z,QString("test"),Qt::black);
                    }
                }
            }
            glEnd();
         }

         if ( theElement->m_type == Surface ){
            // Set the polygon filling mode
            if (theElement->m_fill)
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            else
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            if (theElement->m_flat)
                glShadeModel(GL_FLAT);
            else
                glShadeModel(GL_SMOOTH);

            // Turn on the primitive connection mode (not connected)
            glBegin (GL_QUADS);

            int i, npt = sqrt(theElement->m_pointList.size());

            qglColor(theElement->m_lineColor);

            aPoint = theElement->m_pointList.begin();
            for (i=0; i < theElement->m_pointList.size(); i++){
                int j,k,n;
                if (ptInRange(theElement->m_pointList[i])){
                    // i, j, k, n  4 indices of a quad

                        j = i + npt; // Other vertices indices
                        k = j+1;
                        n = i+1;

                    // Get coordinates of 4 vertices
                    CPoint3D
                        iPt = grid->convertCoord(theElement->m_pointList[i]),
                        jPt = grid->convertCoord(theElement->m_pointList[j]),
                        kPt = grid->convertCoord(theElement->m_pointList[k]),
                        nPt = grid->convertCoord(theElement->m_pointList[n]);

                    float
                        // Quad side lines vectors coordinates
                        ax = iPt.x-nPt.x,
                        ay = iPt.y-nPt.y,

                        by = jPt.y-iPt.y,
                        bz = jPt.z-iPt.z,

                        // Normal vector coordinates
                        vx = ay*bz,
                        vy = -bz*ax,
                        vz = ax*by,

                        // Normal vector length
                        v  = float(sqrt(vx*vx + vy*vy + vz*vz));

                    // Scale to unity
                    vx /= v;
                    vy /= v;
                    vz /= v;

                    // Set the normal vector
                    glNormal3f (vx,vy,vz);

                    //glColor3f (0.2f, 0.8f, 1.f);
                    if (ptInAxisBox(iPt))
                        glVertex3f (iPt.x, iPt.y, iPt.z);

                    //glColor3f (0.6f, 0.7f, 1.f);
                    if (ptInAxisBox(jPt))
                        glVertex3f (jPt.x, jPt.y, jPt.z);

                    //glColor3f (0.7f, 0.9f, 1.f);
                    if (ptInAxisBox(kPt))
                        glVertex3f (kPt.x, kPt.y, kPt.z);

                    //glColor3f (0.7f, 0.8f, 1.f);
                    if (ptInAxisBox(nPt))
                        glVertex3f (nPt.x, nPt.y, nPt.z);
                }
            }
            glEnd();
         }

        if(glIsEnabled(GL_LIGHTING))
            glDisable(GL_LIGHTING);
    }
}

void OpenGLWindow::mousePressEvent(QMouseEvent *event)
{
    m_mouseCaptured = true;
    m_xPos = event->pos().x();
    m_yPos = event->pos().y();
    if(event->button() == Qt::LeftButton)
        m_trackState = Rotate;
    else if(event->button() == Qt::RightButton)
        m_trackState = Zoom;
}

void OpenGLWindow::mouseReleaseEvent(QMouseEvent *event){
    m_mouseCaptured = false;
}

void OpenGLWindow::mouseMoveEvent(QMouseEvent *event)
{
    if (m_mouseCaptured){
        int xPos = event->pos().x(); // horizontal position of cursor
        int yPos = event->pos().y(); // vertical position of cursor

        if (m_trackState == Zoom)
        {
            m_scaling += (float)(m_xPos - xPos)/100.0f;
        }
        else if (m_trackState == Rotate && is3D)
        {
            m_rotationX -= (float)(m_yPos - yPos)/2.0f;
            m_rotationY -= (float)(m_xPos - xPos)/2.0f;
        }

        m_xPos = xPos;
        m_yPos = yPos;
        update();
    }
}

void OpenGLWindow::wheelEvent(QWheelEvent *event)
{
    double numDegrees = -event->delta() / 8.0;
    double numSteps = numDegrees / 15.0;
    //cout<<"numsteps: "<<numSteps<<endl;
    double** ranges = grid->getRanges();
    double newRangeXMin, newRangeXMax, newRangeYMin, newRangeYMax, newRangeZMin, newRangeZMax;
    newRangeXMin = ranges[0][MIN]+ ranges[0][MIN]*numSteps/50;
    newRangeXMax = ranges[0][MAX]+ ranges[0][MAX]*numSteps/50;
    newRangeYMin = ranges[1][MIN]+ ranges[1][MIN]*numSteps/50;
    newRangeYMax = ranges[1][MAX]+ ranges[1][MAX]*numSteps/50;
    newRangeZMin = ranges[2][MIN]+ ranges[2][MIN]*numSteps/50;
    newRangeZMax = ranges[2][MAX]+ ranges[2][MAX]*numSteps/50;
    grid->setRange(newRangeXMin,newRangeXMax,newRangeYMin,newRangeYMax,newRangeZMin,newRangeZMax);
    update();
}

bool OpenGLWindow::getElementLabel(int id){
    return m_ElementList[id].m_label;
}

void OpenGLWindow::setElementLabel(int id, bool newVal){
    m_ElementList[id].m_label = newVal;
    update();
}


void OpenGLWindow::setDisplayGrid(bool newVal){
    showGrid = newVal;
    update();
}

int OpenGLWindow::getProjectionType(){
    return m_projection;
}

void OpenGLWindow::setProjectionType(int t){
    m_projection = (ProjectionStyle)t;
}

QColor OpenGLWindow::getElementLineColor(int id)
{
    return m_ElementList[id].m_lineColor;
}

void OpenGLWindow::setElementLineColor(int id, QColor newVal)
{
    m_ElementList[id].m_lineColor = newVal;
    update();
}

QColor OpenGLWindow::getElementPointColor(int id)
{
    return m_ElementList[id].m_pointColor;
}

void OpenGLWindow::setElementPointColor(int id, QColor newVal)
{
    m_ElementList[id].m_pointColor = newVal;
    update();
}

GLfloat OpenGLWindow::getElementLineWidth(int id)
{
    return m_ElementList[id].m_lineWidth;
}

void OpenGLWindow::setElementLineWidth(int id, float newVal)
{
    m_ElementList[id].m_lineWidth = newVal;
    update();
}

GLfloat OpenGLWindow::getElementPointSize(int id)
{
    return m_ElementList[id].m_pointSize;
}

void OpenGLWindow::setElementPointSize(int id, float newVal)
{
    m_ElementList[id].m_pointSize = newVal;
    update();
}

int OpenGLWindow::getElementType(int id)
{
    return m_ElementList[id].m_type;
}

void OpenGLWindow::setElementType(int id, int newVal)
{
    m_ElementList[id].m_type = (LineType)newVal;
    update();
}

bool OpenGLWindow::getElementShow(int id)
{
    return m_ElementList[id].m_show;
}

void OpenGLWindow::setElementShow(int id, bool newVal)
{
    m_ElementList[id].m_show = newVal;
    update();
}

bool OpenGLWindow::getElementSurfaceFill(int id)
{
    return m_ElementList[id].m_fill;
}

void OpenGLWindow::setElementSurfaceFill(int id, bool newVal)
{
    m_ElementList[id].m_fill = newVal;
    update();
}

bool OpenGLWindow::getElementSurfaceFlat(int id)
{
    return m_ElementList[id].m_flat;
}

void OpenGLWindow::setElementSurfaceFlat(int id, bool newVal)
{
    m_ElementList[id].m_flat = newVal;
    update();
}

bool OpenGLWindow::getElementLight(int id)
{
    return m_ElementList[id].m_lights;
}

void OpenGLWindow::setElementLight(int id, bool newVal)
{
    m_ElementList[id].m_lights = newVal;
    update();
}

void OpenGLWindow::qglColor(QColor color) {
    glColor4f(color.redF(), color.greenF(), color.blueF(), color.alphaF());
}

void OpenGLWindow::qglClearColor(QColor clearColor) {
    glClearColor(clearColor.redF(), clearColor.greenF(), clearColor.blueF(), clearColor.alphaF());
}
